# __      __         _
# \ \    / /        | |
#  \ \  / /_ _ _ __ | |__  _   _ _ __
#   \ \/ / _` | '_ \| '_ \| | | | '__| Valentín Ros Duart (Vanhyr)
#    \  / (_| | | | | | | | |_| | |    https://gitlab.com/vanhyr/
#     \/ \__,_|_| |_|_| |_|\__, |_|
#                           __/ |
#                          |___/
#
# My bash config.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

### VARS ###

## CONFIGS ##

# Aliasrc
aliasrc="${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"

# Profile
profile="${XDG_CONFIG_HOME:-$HOME/.config}/shell/profile"

### SIZE ###
shopt -s checkwinsize

### HISTORY ###
shopt -s histappend
HISTCONTROL=ignoreboth  # Ignore duplicates and lines with space
HISTSIZE=100000
HISTFILESIZE=100000
HISTFILE=$HOME/.cache/bash/history

### PROMPT ###
#PS1="\[\033[38;5;208m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;206m\]\h\[$(tput sgr0)\]:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]\[$(tput                   sgr0)\]\[\033[38;5;34m\]\$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\] - \A\n\[$(tput sgr0)\]\[\033[38;5;               229m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]"
PS1="\[\033[38;5;203m\][\[$(tput sgr0)\]\[\033[38;5;229m\]\u\[$(tput sgr0)\]\[\033[38;5;10m\]@\[$(tput sgr0)\]\[\033[38;5;12m\]\h\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;13m\]\w\[$(tput sgr0)\]\[\033[38;5;203m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;166m\]\$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\] \A\n\[$(tput sgr0)\]\[\033[38;5;231m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]"

### SOURCE ###

## CONFIGS ##

# Aliasrc
[ -f $aliasrc ] && source $aliasrc

# Profile
[ -f $aliasrc ] && source $aliasrc

# Starship
[ -f "/usr/bin/starship" ] && eval "$(starship init bash)" 2>/dev/null
#[ -f "/usr/bin/starship" ] && eval "$(starship init $0)" 2>/dev/null
