" General
set number " Show line numbers
"set linebreak " Break lines at word (requires Wrap lines)
"set showbreak=+++ " Wrap-broken line prefix
"set textwidth=100 " Line wrap (number of cols)
set showmatch " Highlight matching brace
set visualbell " Use visual bell (no beeping)
 
set hlsearch " Highlight all search results
set smartcase " Enable smart-case search
set ignorecase " Always case-insensitive
set incsearch " Searches for strings incrementally
 
set autoindent " Auto-indent new lines
set shiftwidth=2 " Number of auto-indent spaces
set smartindent " Enable smart-indent
set smarttab " Enable smart-tabs
set softtabstop=2 " Number of spaces per Tab
set expandtab " Spaces instead of Tab
"set showtabline=4 " File name ?
set encoding=UTF-8 " UTF-8 encoding

" Advanced
set ruler " Show row and column ruler information
 
set undolevels=1000 " Number of undo levels
set backspace=indent,eol,start " Backspace behaviour

" RE-MAPS "
" Alt+j -> moves down line
nnoremap <A-j> :m .+1<CR>==
" Alt+k -> moves up line
nnoremap <A-k> :m .-2<CR>==
" Alt+j -> moves down visual
vnoremap <A-j> :m '>+1<CR>gv=gv
" Alt+k -> moves up visual
vnoremap <A-k> :m '<-2<CR>gv=gv
" Alt+Ctrl+j -> 
nnoremap <A-C-j> yyp
" Alt+Ctrl+k ->
nnoremap <A-C-k> yyP

" Download and install vim-plug if not installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins (vim-plug)
call plug#begin(system('echo -n "${XDG_CONFIG_HOME}/nvim/plugged"'))

Plug 'tiagovla/tokyodark.nvim' " Tokyo dark theme
Plug 'rakr/vim-one' " One theme
Plug 'ryanoasis/vim-devicons' " Icons
Plug 'vim-airline/vim-airline' " Status line
"Plug 'itchyny/lightline.vim' " Status line (lightweight)
"Plug 'nvim-lualine/lualine.nvim' " Status line (lua)
Plug 'mhinz/vim-startify' " Start page
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' } " Tree view for files
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'  " Tree syntax highlight
"Plug 'vimwiki/vimwiki'  " Notes
Plug 'ellisonleao/glow.nvim', {'branch': 'main'} " Glow (preview markdown)
"Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' } " Markdown live preview (requires yarn and nodejs)
Plug 'sheerun/vim-polyglot' " Syntax highlighting
Plug 'neoclide/coc.nvim', {'branch': 'release'} " Intellisense (requires nodejs)
Plug 'mattn/emmet-vim' " Emmet
Plug 'honza/vim-snippets' " Snippets
Plug 'alvan/vim-closetag', " Auto close html tags
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' } " Color preview (requires go)
Plug 'junegunn/goyo.vim', { 'for': 'markdown' } " No distraction mode

call plug#end()

" Support 24-bit color
execute "set t_8f=\e[38;2;%lu;%lu;%lum"
execute "set t_8b=\e[48;2;%lu;%lu;%lum"
if (has("termguicolors"))
	set termguicolors
endif

" Plugins config "

" Theme
colorscheme tokyodark
"colorscheme one

" Lightline
"set background=dark
"let g:lightline = {'colorscheme' : 'tokyonight'}
"set laststatus=2
"set noshowmode

" Airline
let g:airline_theme='one'
set laststatus=2
set noshowmode " Prevents default mode indicator from showing

" Hexokinase
"let g:Hexokinase_highlighters = ['virtual'] " Show colors in a box at then end of line
let g:Hexokinase_highlighters = ['backgroundfull'] " Show colors behind
"let g:Hexokinase_ftEnabled = ['css', 'html', 'javascript', 'yaml'] " Enable for specific languages
let g:Hexokinase_ftDisabled = ['md', 'markdown'] "Disable for specific languages
" Emmet
let g:user_emmet_mode='n'
let g:user_emmet_leader_key=','

" Coc (intellisense)
" TODO -> coc-snippets throws an error
let g:coc_global_extensions = [
  \ 'coc-snippets',
  \ 'coc-html',
  \ 'coc-css',
  \ 'coc-json',
\ ]

" Wiki
"let g:vimwiki_list = [{'path': '~/documents/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}] " Set path and file type to markdown

" Markdown Preview
let g:mkdp_browser = 'firefox' " Browser to use
let g:mkdp_auto_start = 0 " Auto open preview in browser
let g:mkdp_auto_close = 1 " Auto close
let g:mkdp_refresh_slow = 0 " Auto refresh
let g:mkdp_markdown_css = '~/.config/nvim/markdown/github-markdown-dark.css' " Markdown css to style like github
let g:mkdp_page_title = '${name}' " Browser name to display
let g:mkdp_filetypes = ['markdown'] " Type of files
"let g:mkdp_theme = 'dark' " Theme, default to system
nmap <C-s> <Plug>MarkdownPreview
