----------
-- INIT --
----------

-- Plugins load
require "user.plugins" -- loads packer and my plugins, should be at the top
-- Core
require "user.core.options"
require "user.core.keybindings"
require "user.core.colorscheme"
-- Plugins configs
--require "user.plugins."
