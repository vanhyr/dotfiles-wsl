-------------------
-- PLUGINS SETUP --
-------------------

-- Bootstrap packer (autoinstall)
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- Autocommand to reload neovim whenever you save this file and install,update or remove plugins
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Protected call to import packer
local status, packer = pcall(require, "packer")
if not status then
  return
end

-- Packer plugins import calls
return packer.startup(function(use)
  use("wbthomason/packer.nvim")
  -- My plugins here
  
  -- colorschemes
  use ("bluz71/vim-nightfly-guicolors") -- nightfly color scheme
  use { "catppuccin/nvim", as = "catppuccin" } -- catpuccin color scheme
  use ("rebelot/kanagawa.nvim") -- kanagawa color scheme
  use ("drewtempelmeyer/palenight.vim") -- palenight color scheme
  use ("folke/tokyonight.nvim") -- tokyonight color scheme
  use ({ "rose-pine/neovim", as = "rose-pine" }) -- rose-pine color scheme
  use ("olimorris/onedarkpro.nvim") -- onedarkpro color scheme
  use ("Mofiqul/dracula.nvim") -- dracula color theme

  -- programming


  if packer_bootstrap then
    require("packer").sync()
  end
end)
