-------------
-- Options --
-------------

-- API aliases --
local opt = vim.opt
local o = vim.o -- better than vim.opt?

-- Apply options --

-- appearance
o.termguicolors = true -- enable 24-bit color
o.background = "dark" -- default themes to dark

-- gui
o.title = true -- title
o.number = true -- show line numbers
o.numberwidth = 2 -- numbers width
o.relativenumber = true -- show relative line numbers
o.cursorline = true -- highlight the current line
o.ruler = true -- show row and column ruler info (default is true)
o.scrolloff = 8 -- vertical screen lines offset
o.sidescrolloff = 8 -- horizontal screen line offset 
o.signcolumn = "yes" -- enables sign column
o.showtabline = 2 -- always show tabs
o.showcmd = true -- show the cmd
o.cmdheight = 1 -- cmd height
--o.wrap = true -- wrap lines to be more legible
--o.textwidth = 300 -- wrap when width matches 300 (depends on wrap = true)
o.wrap = false -- don't wrap lines, useful for programming
o.autoindent = true -- auto-indent new lines
o.smarttab = true -- enable smart-tabs
o.smartindent = true -- enable smart-indent
--o.cindent = true -- enable smart-indent c style
o.tabstop = 2 -- number of spaces per tab
o.shiftwidth = 2 -- number of auto-indent spaces
o.softtabstop = -1 -- if negative, shiftwidth value is used
o.expandtab = true -- spaces instead of tabs
o.showmatch = true -- highlight matching brace
o.conceallevel = 0 -- so that `` is visible in markdown files
o.visualbell = true -- use visual bell (no beeping)

-- clipboard
o.clipboard = "unnamedplus" -- allows access to the system clipboard

-- split
o.splitright = true -- force all vertical splits (:vsplit) to go to the right of current window
o.splitbelow = true -- force all horizontal splits (:split) to go below current window

-- search
o.ignorecase = true -- always case-insensitive
o.smartcase = true -- enable smart-case search (depends on ignorecase = true)
o.hlsearch = true -- highlight all search results
--o.incsearch = true -- searches for strings incrementally
o.hlsearch = false -- don't highlight all search results
o.incsearch = true -- searches for strings incrementally

-- encoding
vim.scriptencoding = "utf-8" -- utf-8 script encoding
o.encoding = "utf-8" -- utf-8 encoding
o.fileencoding = "utf-8" -- the encoding written to a file

-- undo, backup and history
o.undofile = true -- enable persistent undo
--o.undolevels = 1000 -- number of undo levels (default is 1000)
o.swapfile = false -- disable swap file
o.backup = false -- creates a backup file
o.writebackup = false -- creates a backup file when writing
--o.history = 50 -- remember x items in commandline history (default is 10000)

-- fixes
opt.backspace = {"indent", "eol", "start"} -- fix backspace behaviour
--o.backspace = 2 -- fix backspace behaviour, same as -> indent,eol,start
opt.iskeyword:append("-") -- treats - separated strings as a single word

-- mouse
o.mouse = "a" -- allow the mouse to be used in all modes
--o.mouse = "n" -- allow the mouse to be used only in normal mode
--o.mouse = "" -- disable mouse

-- decrease update time
o.timeoutlen = 500 -- milliseconds to wait for a key code to complete (default is 1000)
o.updatetime = 200 -- milliseconds for the swap file to be written to disk if nothing is typed (default is 4000)
--o.updatetime = 50 -- milliseconds for the swap file to be written to disk if nothing is typed (default is 4000)
