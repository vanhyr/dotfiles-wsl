-----------------
-- KEYBINDINGS --
-----------------

-- API aliases --
local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
local g = vim.g

-- Modes ---
-- normal_mode = "n",
-- insert_mode = "t",
-- visual_mode = "v",
-- visual__block_mode = "x",
-- term_mode = "t",
-- command_mode = "c",

-- Mappings --

-- Leader --
keymap("", "<Space>", "<Nop>", opts)
g.mapleader = " "
g.maplocalleader = " "

-- Normal --
-- better window navigation
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-l>", "<C-w>l", opts)
keymap("n", "<C-h>", "<C-w>h", opts)
-- open left hand explorer
keymap("n", "<Leader>e", "<CMD>Lex 30<CR>", opts)
-- resize with arrows
keymap("n", "<C-Up>", "<CMD>resize +2<CR>", opts)
keymap("n", "<C-Down>", "<CMD>resize -2<CR>", opts)
keymap("n", "<C-Left>", "<CMD>vertical resize +2<CR>", opts)
keymap("n", "<C-Right>", "<CMD>vertical resize -2<CR>", opts)
-- navigate buffers
keymap("n", "<S-l>", "<CMD>bnext<CR>", opts)
keymap("n", "<S-h>", "<CMD>bprevious<CR>", opts)
-- move lines up and down
keymap("n", "<A-j>", "<CMD>m .+1<CR>==", opts)
keymap("n", "<A-k>", "<CMD>m .-2<CR>==", opts)
-- copy the line up and down
keymap("n", "<A-C-j>", "yyp", opts)
keymap("n", "<A-C-k>", "yyP", opts)
-- stop highlighting search results
keymap("n", "<Leader>nh", "<CMD>nohl<CR>", opts)
-- don't yank when deleting a single character
keymap("n", "x", "_x", opts)
-- split windows
keymap("n", "<Leader>sv", "<C-w>v", opts) -- split window vertically
keymap("n", "<Leader>sh", "<C-w>s", opts) -- split window horizontally
keymap("n", "<Leader>se", "<C-w>=", opts) -- make the split windows equal width
keymap("n", "<Leader>sx", "<CMD>close<CR>", opts) -- close current split window
-- tabs
keymap("n", "<Leader>to", "<CMD>tabnew<CR>", opts) -- open new tab
keymap("n", "<Leader>tx", "<CMD>tabclose<CR>", opts) -- close current tab
keymap("n", "<Leader>tn", "<CMD>tabn<CR>", opts) -- go to next tab
keymap("n", "<Leader>tp", "<CMD>tabp<CR>", opts) -- go to previous tab

-- Insert --
-- press jk fast to enter
keymap("i", "jk", "<ESC>", opts) -- idk about that
-- navigate with vim keys in insert mode
--keymap("i", "C-h", "<Left>", opts)
--keymap("i", "C-j", "<Down>", opts)
--keymap("i", "C-k", "<Up>", opts)
--keymap("i", "C-l", "<Right>", opts)

-- Visual --
-- don't yank when pasting
keymap("v", "p", '"_dP', opts)
-- stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
-- move lines up and down
keymap("v", "<A-j>", "<CMD>m '>+1<CR>==", opts)
keymap("v", "<A-k>", "<CMD>m '<-2<CR>==", opts)

-- Visual block --
-- move lines up and down
keymap("x", "<A-j>", "<CMD>m '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", "<CMD>m '<-2<CR>gv-gv", opts)
-- disable arrows
--keymap("", "<Up>", "<Nop>", opts)
--keymap("", "<Down>", "<Nop>", opts)
--keymap("", "<Left>", "<Nop>", opts)
--keymap("", "<Right>", "<Nop>", opts)
