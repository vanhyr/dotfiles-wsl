------------------
-- COLOR SCHEME --
------------------

--local colorscheme = "nightfly"
local colorscheme = "catppuccin-mocha"
--local colorscheme = "kanagawa"
--local colorscheme = "palenight"
--local colorscheme = "tokyonight-night" -- available in -> moon, 
--local colorscheme = "rose-pine"
--local colorscheme = "onedark"
--local colorscheme = "dracula"


-- protected call
local status, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status then
  print("Colorscheme not found!")
  return
end

vim.cmd("colorscheme " .. colorscheme)
