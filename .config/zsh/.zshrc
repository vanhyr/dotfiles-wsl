# __	    __         _                 
# \ \    / /        | |                
#  \ \  / /_ _ _ __ | |__  _   _ _ __  
#   \ \/ / _` | '_ \| '_ \| | | | '__| Valentín Ros Duart (Vanhyr)
#    \  / (_| | | | | | | | |_| | |    https://gitlab.com/vanhyr/
#     \/ \__,_|_| |_|_| |_|\__, |_|    
#                           __/ |      
#                          |___/       
#
# My zsh config.

### VARS ###

## DIRS ##

plugins_dir="${XDG_CONFIG_HOME:-$HOME/.config}/zsh/plugins"
themes_dir="${XDG_CONFIG_HOME:-$HOME/.config}/zsh/themes"

## CONFIG FILES ##

aliasrc="${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
profile="${XDG_CONFIG_HOME:-$HOME/.config}/shell/profile"

#p10kzsh="${XDG_CONFIG_HOME:-$HOME/.config}/p10k/.p10k.zsh"

### FUNCTIONS ###

# Source a file
function zsh_src() {
  [ -f "$1" ] && source "$1" 2>/dev/null
}

# Source a plugin
function zsh_src_plug() {
 [ -f "$plugins_dir/$1" ] && source "$plugins_dir/$1" 2>/dev/null
}

# Source a theme
function zsh_src_theme() {
  [ -f "$themes_dir/$1" ] && source "$themes_dir/$1" 2>/dev/null
}

# My own minimal plugin manager
function zsh_plug() {
  plugin_name=$(echo $1 | cut -d "/" -f 2)
  alt_plugin_name=$(echo $plugin_name | cut -d "-" -f 2-)
  if [ -d "$plugins_dir/$plugin_name" ]; then
    zsh_src_plug "$plugin_name/$plugin_name.plugin.zsh" || \
    zsh_src_plug "$plugin_name/$plugin_name.zsh" || \
    zsh_src_plug "$plugin_name/$alt_plugin_name.plugin.zsh" || \
    zsh_src_plug "$plugin_name/$alt_plugin_name.zsh"
  else
    git clone "https://github.com/$1.git" "$plugins_dir/$plugin_name"
  fi
}

# My own minimal themes manager
function zsh_theme() {
  theme_name=$(echo $1 | cut -d "/" -f 2)
  if [ -d "$themes_dir/$theme_name" ]; then
    zsh_src_theme "$theme_name/$theme_name.zsh-theme"
  else
    git clone "https://github.com/$1.git" "$themes_dir/$theme_name"
  fi
}

### HISTORY ###

HISTSIZE=100000
SAVEHIST=100000
HISTFILE=$HOME/.cache/zsh/history

### PROMPT ###

autoload -U colors && colors # load colors.
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

### Colors ###

#typeset -A ZSH_HIGHLIGHT_STYLES
#ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=magenta'
#ZSH_HIGHLIGHT_STYLES[precommand]='fg=magenta'
#ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=magenta'
#ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red'
#ZSH_HIGHLIGHT_STYLES[redirection]='fg=cyan'
#ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=cyan'
#ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=blue'
#ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=blue'
#ZSH_HIGHLIGHT_STYLES[path]='fg=blue'

### OPTIONS ###

setopt autocd	# automatically cd into typed dir
setopt interactive_comments	# allow comments after commad
#unsetopt PROMPT_SP # stop % from appearing (not sure)
stty stop undef	# disable ctrl-s to freeze terminal

### TAB AUTOCOMPLETE ###

zstyle ':completion:*' menu select
#zstyle ':completion:*' matcher-list '' \
#	'm:{a-z\-}={A-Z\_}' \
#	'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
#	'r:|?=** m:{a-z\-}={A-Z\_}etopt interactive_comments'
zmodload zsh/complist
autoload -Uz compinit
compinit
_comp_options+=(globdots)	# include hidden files

### SOURCE ###

## PLUGINS ##

zsh_plug "zsh-users/zsh-autosuggestions"
#zsh_plug "catppuccin/zsh-syntax-highlighting" # does not work
zsh_plug "zsh-users/zsh-syntax-highlighting"
zsh_plug "zsh-users/zsh-history-substring-search"
#zsh_plug "MichaelAquilina/zsh-auto-notify"
zsh_plug "MichaelAquilina/zsh-you-should-use"

## THEMES ##
#zsh_theme "romkatv/powerlevel10k"

## CONFIGS ##

# History substring search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
# In case above doesn't work
#bindkey "$terminfo[kcuu1]" history-substring-search-up
#bindkey "$terminfo[kcud1]" history-substring-search-down

# Auto notify
#AUTO_NOTIFY_IGNORE+=("lf")

# Aliasrc
zsh_src "$aliasrc"

# Profile
zsh_src "$profile"

# P10k
#zsh_src "$p10kzsh"

# Starship
[ -f "/usr/bin/starship" ] && eval "$(starship init zsh)" 2>/dev/null
#[ -f "/usr/bin/starship" ] && eval "$(starship init $0)" 2>/dev/null
